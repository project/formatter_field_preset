<?php

declare(strict_types=1);

namespace Drupal\formatter_field_preset\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\formatter_field_preset\Entity\FormatterFieldPreset;
use Drupal\formatter_field_preset\Entity\FormatterFieldPresetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a formatter for the 'formatter_field_formatter' field type.
 *
 * @FieldWidget(
 *   id = "formatter_field_preset",
 *   label = @Translation("Formatter from preset"),
 *   field_types = {"formatter_field_formatter"},
 * )
 */
class FormatterFieldPresetWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $presets = FormatterFieldPreset::loadByFormatterField(
      $items->getEntity()->getEntityTypeId(),
      $items->getEntity()->bundle(),
      $items->getName(),
    );

    $defaultValue = NULL;
    foreach ($presets as $preset) {
      if ($preset->getFormatterFieldDefinition()->getName() === $items->getName()) {
        $defaultValue = $preset->id();
      }
    }

    $element['preset'] = [
      '#type' => 'select',
      '#title' => $this->t('Preset'),
      '#default_value' => $defaultValue,
      '#options' => array_map(
        fn(FormatterFieldPresetInterface $preset): string => $preset->label(),
        $presets,
      ),
      '#empty_value' => NULL,
      '#empty_option' => $this->t('- Select -'),
      '#required' => TRUE,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\formatter_field_preset\Entity\FormatterFieldPresetInterface $preset */
    $preset = $this->entityTypeManager->getStorage('formatter_field_preset')->load($values[0]['preset']);
    return [
      [
        'type' => $preset->getFormatterId(),
        'settings' => serialize($preset->getFormatterSettings()),
        'third_party_settings' => serialize($preset->getFormatterThirdPartySettings()),
      ],
    ];
  }

}
