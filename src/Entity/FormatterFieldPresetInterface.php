<?php

declare(strict_types=1);

namespace Drupal\formatter_field_preset\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Interface for formatter_field_preset entities.
 */
interface FormatterFieldPresetInterface extends ConfigEntityInterface {

  /**
   * Returns the field definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   *   The field definition.
   */
  public function getFieldDefinition(): ?FieldDefinitionInterface;

  /**
   * Returns the formatter plugin ID.
   *
   * @return string|null
   *   The formatter plugin ID.
   */
  public function getFormatterId(): ?string;

  /**
   * Returns the formatter plugin settings.
   *
   * @return array
   *   The formatter plugin settings.
   */
  public function getFormatterSettings(): array;

  /**
   * Returns the formatter plugin third-party settings.
   *
   * @return array
   *   The formatter plugin third-party settings.
   */
  public function getFormatterThirdPartySettings(): array;

  /**
   * Returns the formatter field definition for this preset.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|null
   *   The formatter field definition.
   */
  public function getFormatterFieldDefinition(): ?FieldDefinitionInterface;

  /**
   * Loads presets designed for a given formatter field.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   * @param string $bundle
   *   The bundle.
   * @param string $fieldName
   *   The formatter field name.
   *
   * @return \Drupal\formatter_field_preset\Entity\FormatterFieldPresetInterface[]
   *   A list of presets.
   */
  public static function loadByFormatterField(string $entityTypeId, string $bundle, string $fieldName): array;

}
