<?php

declare(strict_types=1);

namespace Drupal\formatter_field_preset\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Defines the Node type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "formatter_field_preset",
 *   label = @Translation("Formatter field preset"),
 *   label_collection = @Translation("Formatter field presets"),
 *   label_singular = @Translation("formatter field preset"),
 *   label_plural = @Translation("formatter field presets"),
 *   label_count = @PluralTranslation(
 *     singular = "@count formatter field preset",
 *     plural = "@count formatter field presets",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\formatter_field_preset\Form\FormatterFieldPresetForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\formatter_field_preset\FormatterFieldPresetListBuilder",
 *   },
 *   admin_permission = "administer content types",
 *   config_prefix = "preset",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/content/formatter-field-preset/add",
 *     "edit-form" = "/admin/config/content/formatter-field-preset/manage/{formatter_field_preset}",
 *     "delete-form" = "/admin/config/content/formatter-field-preset/manage/{formatter_field_preset}/delete",
 *     "collection" = "/admin/config/content/formatter-field-preset",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "field",
 *     "formatter",
 *   },
 * )
 */
class FormatterFieldPreset extends ConfigEntityBase implements FormatterFieldPresetInterface {

  /**
   * The field instance.
   *
   * Represented as $entityTypeId.$bundle.$fieldName.
   */
  protected ?string $field = NULL;

  /**
   * Formatter data.
   */
  protected array $formatter = [
    'id' => NULL,
    'settings' => [],
    'third_party_settings' => [],
  ];

  /**
   * The formatter field definition.
   */
  protected FieldDefinitionInterface $formatterFieldDefinition;

  /**
   * The entity field manager service.
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinition(): ?FieldDefinitionInterface {
    if ($this->field) {
      [$entityTypeId, $bundle, $fieldName] = explode('.', $this->field);
      return $this->getFieldManager()->getFieldDefinitions($entityTypeId, $bundle)[$fieldName];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatterId(): ?string {
    return $this->formatter['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatterSettings(): array {
    return $this->formatter['settings'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatterThirdPartySettings(): array {
    return $this->formatter['third_party_settings'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatterFieldDefinition(): ?FieldDefinitionInterface {
    if (!isset($this->formatterFieldDefinition)) {
      foreach ($this->getFieldManager()->getFieldMapByFieldType('formatter_field_formatter') as $entityTypeId => $fields) {
        foreach ($fields as $fieldName => $fieldInfo) {
          foreach ($fieldInfo['bundles'] as $bundle) {
            $definition = $this->getFieldManager()->getFieldDefinitions($entityTypeId, $bundle)[$fieldName];
            if ($definition->getSetting('field') === $this->getFieldDefinition()->getName()) {
              $this->formatterFieldDefinition = $definition;
              break 3;
            }
          }
        }
      }
      if (!isset($this->formatterFieldDefinition)) {
        throw new \RuntimeException('Formatter field definition cannot be computed');
      }
    }
    return $this->formatterFieldDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public static function loadByFormatterField(string $entityTypeId, string $bundle, string $fieldName): array {
    return array_filter(
      \Drupal::entityTypeManager()->getStorage('formatter_field_preset')->loadMultiple(),
      function (FormatterFieldPresetInterface $preset) use ($entityTypeId, $bundle, $fieldName): bool {
        $definition = $preset->getFormatterFieldDefinition();
        return $definition->getTargetEntityTypeId() === $entityTypeId &&
          $definition->getTargetBundle() === $bundle &&
          $definition->getName() === $fieldName;
      },
    );
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): FormatterFieldPresetInterface {
    parent::calculateDependencies();

    $definition = $this->getFieldDefinition();
    if ($definition instanceof FieldConfigInterface) {
      $this->addDependency($definition->getConfigDependencyKey(), $definition->getConfigDependencyName());
    }

    $definition = $this->getFormatterFieldDefinition();
    if ($definition instanceof FieldConfigInterface) {
      $this->addDependency($definition->getConfigDependencyKey(), $definition->getConfigDependencyName());
    }

    return $this;
  }


  protected function getFieldManager(): EntityFieldManagerInterface {
    if (!isset($this->entityFieldManager)) {
      $this->entityFieldManager = \Drupal::service('entity_field.manager');
    }
    return $this->entityFieldManager;
  }

}
