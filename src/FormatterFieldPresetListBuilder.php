<?php

declare(strict_types=1);

namespace Drupal\formatter_field_preset;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\formatter_field_preset\Entity\FormatterFieldPreset;
use Drupal\formatter_field_preset\Entity\FormatterFieldPresetInterface;

/**
 * List builder for `formatter_field_preset` config entity.
 */
class FormatterFieldPresetListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'preset' => t('Preset'),
      'field' => t('Field'),
      'formatter' => t('Formatter'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    assert($entity instanceof FormatterFieldPresetInterface);
    return [
      'preset' => $entity->label(),
      'field' => $entity->getFieldDefinition()->id(),
      'formatter' => $entity->getFormatterId(),
    ] + parent::buildRow($entity);
  }

}
