<?php

declare(strict_types=1);

namespace Drupal\formatter_field_preset\Form;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Field\FormatterPluginManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\formatter_field_preset\Entity\FormatterFieldPreset;
use Drupal\formatter_field_preset\Entity\FormatterFieldPresetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Formatter field preset entity form.
 */
class FormatterFieldPresetForm extends EntityForm {

  use AjaxFormHelperTrait;

  public function __construct(
    protected EntityFieldManagerInterface $entityFieldManager,
    protected EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected FormatterPluginManager $formatterManager,
  ) {}

  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('plugin.manager.field.formatter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->getEntity()->label(),
      '#required' => TRUE,
      '#maxlength' => 255,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('ID'),
      '#default_value' => $this->getEntity()->id(),
      '#maxlength' => 255,
      '#disabled' => !$this->getEntity()->isNew(),
      '#machine_name' => [
        'exists' => [FormatterFieldPreset::class, 'load'],
        'source' => ['label'],
      ],
    ];

    $fieldId = $this->getEntity()->getFieldDefinition()?->id() ?? $form_state->getUserInput()['field'] ?? NULL;

    $form['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#options' => $this->getTargetFields(),
      '#default_value' => $fieldId,
      '#required' => TRUE,
      '#ajax' => ['callback' => '::ajaxSubmit'],
      '#disabled' => !$this->getEntity()->isNew(),
    ];

    if (!$fieldId) {
      return $form;
    }

    [$entityTypeId, $bundle, $fieldName] = explode('.', $fieldId);
    $definition = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle)[$fieldName];

    $form['formatter'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $pluginId = $this->getEntity()->getFormatterId() ?? $form_state->getUserInput()['formatter']['id'] ?? NULL;

    $options = $this->formatterManager->getOptions($definition->getType());
    $applicableOptions = ['' => $this->t('- Hidden -')];
    foreach ($options as $option => $label) {
      /** @var \Drupal\Core\Field\FormatterInterface $pluginClass */
      $pluginClass = DefaultFactory::getPluginClass($option, $this->formatterManager->getDefinition($option));
      if ($pluginClass::isApplicable($definition) && $option !== 'formatter_field_from') {
        $applicableOptions[$option] = $label;
      }
    }

    $form['formatter']['id'] = [
      '#type' => 'select',
      '#title' => $this->t('Formatter'),
      '#default_value' => $pluginId,
      '#options' => $applicableOptions,
      '#required' => TRUE,
      '#ajax' => ['callback' => '::ajaxSubmit'],
      '#disabled' => !$this->getEntity()->isNew(),
    ];

    if ($pluginId) {
      $this->getPluginSettingsForm($pluginId, $definition, $form, $form_state);
    }

    if ($this->isAjax()) {
      // @see https://www.drupal.org/node/2897377
      $form['#id'] = Html::getId($form_state->getBuildInfo()['form_id']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): FormatterFieldPresetInterface {
    $entity = parent::getEntity();
    assert($entity instanceof FormatterFieldPresetInterface);
    return $entity;
  }


  protected function successfulAjaxSubmit(
    array $form,
    FormStateInterface $form_state
  ): AjaxResponse {
    $selector = '[data-drupal-selector="' . $form['#attributes']['data-drupal-selector'] . '"]';
    return (new AjaxResponse())->addCommand(new ReplaceCommand($selector, $form));

  }

  protected function getTargetFields(): array {
    $targetFields = [];
    foreach ($this->entityFieldManager->getFieldMapByFieldType('formatter_field_formatter') as $entityTypeId => $fields) {
      foreach ($fields as $fieldName => $fieldInfo) {
        foreach ($fieldInfo['bundles'] as $bundle) {
          $definitions = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);
          $targetFieldName = $definitions[$fieldName]->getSetting('field');
          $targetDefinition = $definitions[$targetFieldName];
          $targetFields["$entityTypeId.$bundle.$targetFieldName"] = $this->t('@type » @bundle » @name', [
            '@type' => $this->entityTypeManager->getDefinition($entityTypeId)->getLabel(),
            '@bundle' => $this->entityTypeBundleInfo->getBundleInfo($entityTypeId)[$bundle]['label'],
            '@name' => $targetDefinition->getLabel(),
          ]);
        }
      }
    }
    return $targetFields;
  }

  protected function getPluginSettingsForm(string $pluginId, FieldDefinitionInterface $definition, array &$form, FormStateInterface $formState): void {
    $options = [
      'field_definition' => $definition,
      'configuration' => [
        'type' => $pluginId,
        'settings' => $this->getEntity()->getFormatterSettings(),
        'third_party_settings' => $this->getEntity()->getFormatterThirdPartySettings(),
        'label' => '',
        'weight' => 0,
      ],
      'view_mode' => '_custom',
    ];

    $formatter = $this->formatterManager->getInstance($options);

    if ($settingsForm = $formatter->settingsForm($form, $formState)) {
      $form['formatter']['settings'] = [
          '#type' => 'details',
          '#open' => TRUE,
          '#title' => $this->t('@formatter settings', [
            '@formatter' => $formatter->getPluginDefinition()['label'],
          ]),
        ] + $settingsForm;
    }

    $thirdPartySettingsForm = [];
    $this->moduleHandler->invokeAllWith(
      'field_formatter_third_party_settings_form',
      function (callable $hook, string $module) use (&$thirdPartySettingsForm, $formatter, $definition, &$form, $formState) {
        $thirdPartySettingsForm[$module] = $hook(
          $formatter,
          $definition,
          '_custom',
          $form,
          $formState,
        );
      }
    );

    // Remove empty module settings.
    $thirdPartySettingsForm = array_filter($thirdPartySettingsForm);

    if ($thirdPartySettingsForm) {
      $form['formatter']['third_party_settings'] = [
        '#type' => 'container',
      ];
      foreach ($thirdPartySettingsForm as $module => $moduleSettings) {
        $form['formatter']['third_party_settings'][$module] = [
          '#type' => 'details',
          '#open' => TRUE,
          '#title' => $this->t('Settings provided by @module module', [
            '@module' => $this->moduleHandler->getName($module),
          ]),
        ] + $moduleSettings;
      }
    }
  }

}
