<?php

declare(strict_types=1);

namespace Drupal\Tests\formatter_field_preset\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\formatter_field_preset\Entity\FormatterFieldPreset;
use Drupal\formatter_field_preset\Entity\FormatterFieldPresetInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\formatter_field_preset\Entity\FormatterFieldPreset
 * @group formatter_field_preset
 */
class DependencyTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'field',
    'formatter_field',
    'formatter_field_preset',
    'text',
  ];

  /**
   * @covers ::calculateDependencies
   */
  public function testDependencyRemoval(): void {
    $settings = ['trim_length' => 200];

    $formattedField = $this->createFormattedField();
    $formatterField = $this->createFormatterField();

    // Preset.
    $preset = $this->createPreset('foo', $formattedField->getName(), 'text_summary_or_trimmed', $settings);
    $this->assertInstanceOf(FormatterFieldPresetInterface::class, $preset);

    // Check that preset is deleted when the field to be formatted is deleted.
    $formattedField->delete();
    $this->assertNull(FormatterFieldPreset::load('foo'));

    // Recreate the formatted field and the preset.
    $this->createFormattedField();
    $preset = $this->createPreset('foo', $formattedField->getName(), 'text_summary_or_trimmed', $settings);
    $this->assertInstanceOf(FormatterFieldPresetInterface::class, $preset);

    // Check that preset is deleted when the formatter field is deleted.
    $formatterField->delete();
    $this->assertNull(FormatterFieldPreset::load('foo'));
  }

  protected function createFormattedField(): FieldConfigInterface {
    FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'body',
      'type' => 'text_with_summary',
    ])->save();
    $field = FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => 'body',
    ]);
    $field->save();
    return $field;
  }

  protected function createFormatterField(): FieldConfigInterface {
    FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'formatter_field',
      'type' => 'formatter_field_formatter',
    ])->save();
    $field = FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => 'formatter_field',
      'settings' => [
        'field' => 'body',
      ],
    ]);
    $field->save();
    return $field;
  }

  protected function createPreset(string $id, string $formattedFieldName, string $formatterId, array $formatteSettings): FormatterFieldPresetInterface {
    $preset = FormatterFieldPreset::create([
      'id' => $id,
      'label' => $id,
      'field' => "entity_test.entity_test.$formattedFieldName",
      'formatter' => [
        'id' => $formatterId,
        'settings' => $formatteSettings,
      ],
    ]);
    $preset->save();
    return $preset;
  }


}

